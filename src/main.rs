use std::error::Error;

use opencv::{
    core::{Point_, Scalar, Size},
    highgui, imgproc, optflow,
    prelude::*,
    videoio,
};

// We compute the optical flow on a scaled-down
// copy of the frame
const FLOW_SCALE: f32 = 0.2;

// Then we scale down the result further for vector visualisation
const VIS_FLOW_SCALE: f32 = 0.1;

fn prepare(frame: &Mat) -> Mat {
    // Convert to gray, scale down the image
    let mut small = Mat::default();
    let mut gray = Mat::default();
    imgproc::resize(
        &frame,
        &mut small,
        Size::default(),
        FLOW_SCALE.into(),
        FLOW_SCALE.into(),
        imgproc::INTER_LINEAR,
    )
    .unwrap();
    imgproc::cvt_color(&small, &mut gray, imgproc::COLOR_BGR2GRAY, 0).unwrap();
    gray
}

fn make_flow_compute() -> impl FnMut(&Mat, &Mat) -> Mat {
    // Return a closure capturing an optical flow object,
    // taking previous and next frames as arguments and returning
    // a calculated optical flow matrix
    let mut flow_calc = optflow::create_opt_flow_farneback().unwrap();
    move |frame: &Mat, prev: &Mat| {
        let mut flow = Mat::default();
        flow_calc.calc(&prev, &frame, &mut flow).unwrap();
        flow
    }
}

fn arrow_coords(x: i32, y: i32, dx: f32, dy: f32) -> (Point_<i32>, Point_<i32>) {
    // Boring calculation & scaling of visualisation arrow coordinates
    let rscale = FLOW_SCALE.recip();
    let from_x = x as f32 * rscale * VIS_FLOW_SCALE.recip();
    let from_y = y as f32 * rscale * VIS_FLOW_SCALE.recip();
    let to_x = x as f32 * rscale * VIS_FLOW_SCALE.recip() + dx * rscale;
    let to_y = y as f32 * rscale * VIS_FLOW_SCALE.recip() + dy * rscale;

    (
        Point_::new(from_x as i32, from_y as i32),
        Point_::new(to_x as i32, to_y as i32),
    )
}

fn visualize(frame: &Mat, flow: &Mat) -> Mat {
    // Draw arrows over a frame indicating the vectors of the optical flow.
    // We scale the flow matrix down to reduce the density of arrows drawn
    // and to average them over
    let mut vis = frame.clone();
    let mut flow_small = Mat::default();
    imgproc::resize(
        &flow,
        &mut flow_small,
        Size::default(),
        VIS_FLOW_SCALE.into(),
        VIS_FLOW_SCALE.into(),
        imgproc::INTER_NEAREST,
    )
    .unwrap();
    for (Point_ { x, y }, Point_ { x: dx, y: dy }) in flow_small.iter::<Point_<f32>>().unwrap() {
        if dx.abs() > 1.0 || dy.abs() > 1.0 {
            let (from, to) = arrow_coords(x, y, dx, dy);
            imgproc::arrowed_line(
                &mut vis,
                from,
                to,
                Scalar::new(255.0, 255.0, 255.0, 255.0),
                1,
                imgproc::LINE_4,
                0,
                0.15,
            )
            .unwrap();
        }
    }
    vis
}

fn main() -> Result<(), Box<dyn Error>> {
    highgui::named_window("window", highgui::WINDOW_AUTOSIZE)?;

    let mut cam = videoio::VideoCapture::new(0, videoio::CAP_ANY)?;
    let mut full = Mat::default();
    let mut prev = Mat::default();

    let mut flow_calc = make_flow_compute();

    cam.set(videoio::CAP_PROP_FRAME_WIDTH, 1280.0)?;
    cam.set(videoio::CAP_PROP_FRAME_HEIGHT, 720.0)?;

    loop {
        cam.read(&mut full)?;
        let frame = prepare(&full);

        if !prev.empty() {
            let flow = flow_calc(&frame, &prev);
            if !flow.empty() {
                let vis = visualize(&full, &flow);
                highgui::imshow("window", &vis)?;
            }
        }

        prev = frame.clone();

        let key = highgui::wait_key(1)?;
        if key == 113 {
            break;
        }
    }
    Ok(())
}
