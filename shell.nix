with import <nixpkgs> {};

let
  opencvGtk = opencv.override { enableGtk3 = true; };
in
  mkShell {
    buildInputs = [
      cargo
      rustc
      opencvGtk
      clang
      pkg-config
    ];

    nativeBuildInputs = [ rustPlatform.bindgenHook ];
  }
