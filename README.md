# Dense optical flow in Rust

An exercise in using Rust to compute and visualize dense optical flow
with OpenCV.

![Demo video](demo.mp4)

Currently it uses the the calcOpticalFlowFarneback() OpenCV algorithm
with default parameters.

Licensed under [CC0 1.0](http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1)
